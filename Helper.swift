//
//  Helper.swift
//  HelloTime
//
//  Created by Mickael Lamotte on 20/09/2021.
//

import Foundation
import SwiftUI
import UIKit
import AVFoundation

extension UIScreen {
    static let screenWidth = UIScreen.main.bounds.width
    static let screenHeight = UIScreen.main.bounds.height
}

// SIZE GUIDE //

    // TimerView
var titleSize: CGFloat = UIScreen.screenHeight > 740 ? 38 : 26
var paddingTopTitleSize: CGFloat = UIScreen.screenHeight > 740 ? 20 : 10

var subTitleSize: CGFloat = 0
var normalSize: CGFloat = 0
var timerSize: CGFloat = 0
var labelTimerSize: CGFloat = UIScreen.screenHeight > 730 ? 48 : 36
var labelMillisTimerSize: CGFloat = UIScreen.screenHeight > 730 ? 28 : 24

var buttonPlayTimerSize: CGFloat = UIScreen.screenHeight > 730 ? 120 : 80
var buttonStopTimerSize: CGFloat = UIScreen.screenHeight > 730 ? 80 : 60
var paddingClockSize: CGFloat = UIScreen.screenHeight > 660 ? 20 : 0

var buttonHeightSize: CGFloat = UIScreen.screenHeight > 740 ? 70 : 50
var buttonWidthSize: CGFloat = UIScreen.screenHeight > 740 ? 250 : 200
var paddingButtonPlaySize: CGFloat = UIScreen.screenHeight > 740 ? 0 : 20

var navBarSize: CGFloat = UIScreen.screenHeight > 660 ? 20 : 15

var paddingSize: CGFloat = UIScreen.screenHeight > 740 ? 25 : 20

    // AddTimerView

var titleTextFieldSize: CGFloat = UIScreen.screenHeight > 740 ? 25 : 20
var heightTextField: CGFloat = UIScreen.screenHeight > 740 ? 60 : 50

// iPhone 12 : 87
// iPhone Xr : 92

var sizePickerColor: CGFloat = UIScreen.screenHeight > 660 ? 50 : 40
var sizePickerColorSelector: CGFloat = UIScreen.screenHeight > 660 ? 20 : 15
var paddingPickerColor: CGFloat = UIScreen.screenHeight > 660 ? 6 : 4

var sizePickerLabel: CGFloat = UIScreen.screenHeight > 730 ? 60 : 40

var paddingTopTitle: CGFloat = UIScreen.screenHeight > 740 ? 50 : 30
var paddingBottomTitle: CGFloat = UIScreen.screenHeight > 740 ? 50 : 20

var buttonWidthSizex2: CGFloat = UIScreen.screenHeight > 660 ? 150 : 130

var pickerSize: CGFloat = UIScreen.screenHeight > 660 ? 90 : 70

    // MenuView

var titleItemMenuSize: CGFloat = UIScreen.screenHeight > 660 ? 24 : 20
var subtitleItemMenuSize: CGFloat = UIScreen.screenHeight > 660 ? 16 : 14

var offsetSubtitleItemMenu: CGFloat = UIScreen.screenHeight > 660 ? 10 : 8

var messageMenuSize: CGFloat = UIScreen.screenHeight > 660 ? 24 : 22

var plusSize: CGFloat = UIScreen.screenHeight > 660 ? 40 : 30
var bottomPaddingButtonMenuView: CGFloat = UIScreen.screenHeight > 660 ? 100 : 80

var logoDeleteSize: CGFloat = UIScreen.screenHeight > 660 ? 30 : 20
var logoDeleteMinusSize: CGFloat = UIScreen.screenHeight > 660 ? 3 : 2

// SIZE GUIDE //

var audioPlayer: AVAudioPlayer?

func playSound(sound: String, type: String) {
    if let path = Bundle.main.path(forResource: sound, ofType: type) {
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
            audioPlayer?.play()
        } catch {
            print("ERROR : Could not find and play the sound")
        }
    }
}

struct NavigationLinkLazyView<Content: View> : View {
    let build: () -> Content
    init(_ build: @autoclosure @escaping () -> Content) {
        self.build = build
    }
    
    var body: Content {
        build()
    }
}

extension String {
    func localized() -> String {
        return NSLocalizedString(
            self,
            tableName: "Localization",
            bundle: .main,
            value: self,
            comment: self
        )
    }
}

enum TimerMode {
    case initial
    case running
    case paused
    case finished
}

func initTimerString(nbOfMillisecond: Double) -> String {
    let hour = Int(nbOfMillisecond / 360000) < 10
        ? "0\(String(Int(nbOfMillisecond / 360000)))"
        : String(Int(nbOfMillisecond / 360000))
    
    let minute = Int(nbOfMillisecond / 6000 ) % 60 < 10
        ? "0\(String(Int(nbOfMillisecond / 6000 ) % 60))"
        : String(Int(nbOfMillisecond / 6000 ) % 60)
    
    let second = Int(nbOfMillisecond / 100 ) % 60 < 10
        ? "0\(String(Int(nbOfMillisecond / 100 ) % 60))"
        : String(Int(nbOfMillisecond / 100 ) % 60)
    
    return hour == "00" ? "\(minute) : \(second)" : "\(hour) : \(minute) : \(second)"
}

struct BackButton: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var color: String
    
    var body: some View {
        Button(action: {
            UIScrollView.appearance().bounces = true
            self.presentationMode.wrappedValue.dismiss()
        }, label: {
            Image(systemName: "chevron.left")
                .resizable()
                .scaledToFit()
                .frame(width: navBarSize, height: navBarSize, alignment: .center)
                .foregroundColor(Color(color))
                
            Text("Go Back".localized())
                .font(.custom("SFProDisplay-Semibold", size: navBarSize))
                .foregroundColor(Color(color))
        })
        .padding(.init(top: 0, leading: paddingSize, bottom: 0, trailing: 0))
    }
}

struct BackButtonWithTimeManager: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @ObservedObject var timerManager: TimerManager
    
    @ObservedObject var notificationManager: LocalNotificationManager
    
    var body: some View {
        Button(action: {
            self.timerManager.stop()
            self.notificationManager.removeNotification()
            self.presentationMode.wrappedValue.dismiss()
            audioPlayer?.stop()
        }, label: {
            HStack {
                Image(systemName: "chevron.left")
                    .resizable()
                    .scaledToFit()
                    .frame(width: navBarSize, height: navBarSize, alignment: .center)
                    .foregroundColor(Color(timerManager.customTimer.color!))
                    
                Text("Go Back".localized())
                    .font(.custom("SFProDisplay-Semibold", size: navBarSize))
                    .foregroundColor(Color(timerManager.customTimer.color!))
                    
            }
        })
        .padding(.init(top: 0, leading: paddingSize, bottom: 0, trailing: 0))
    }
}

struct RefreshTimer: View {
    
    @ObservedObject var timerManager: TimerManager
    
    var body: some View {
        HStack {
            Text(timerManager.hour)
            Text(":")
            Text(timerManager.minute)
            Text(":")
            Text(timerManager.second)
            VStack {
                Spacer()
                HStack {
                    Text(".")
                        .font(.system(size: 30, design: .monospaced))
                    Text(timerManager.millisecond)
                        .font(.system(size: 30, design: .monospaced))
                }
            }
        }
        .font(.system(size: 60, design: .monospaced))
        .foregroundColor(.white)
        .frame(height: 60, alignment: .center)
        .padding(.bottom, 40)
    }
}

struct ButtonOfferPremium: ButtonStyle {
    
    var isAcquired: Bool
    var width: CGFloat
    var height: CGFloat
    
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .padding(0)
            .contentShape(RoundedRectangle(cornerSize: CGSize(width: 10, height: 10)))
            .background(
                Group {
                    RoundedRectangle(cornerSize: CGSize(width: 10, height: 10))
                        .strokeBorder(Color("BackgroundColor"), lineWidth: 2)
                        .frame(width: width, height: height)
                        .overlay(
                            RoundedRectangle(cornerSize: CGSize(width: 10, height: 10))
                                .stroke(Color.white.opacity(0.16), lineWidth: 8)
                                .blur(radius: 2)
                                .frame(width: width+10, height: height+10)
                                .offset(x: 4, y: 4)
                        )
                        .overlay(
                            RoundedRectangle(cornerSize: CGSize(width: 10, height: 10))
                                .stroke(Color.black.opacity(0.50), lineWidth: 8)
                                .blur(radius: 2)
                                .frame(width: width+10, height: height+10)
                                .offset(x: -4, y: -4)
                        )
                        .background(
                            VStack {
                                if isAcquired {
                                    RoundedRectangle(cornerRadius: 10).fill(Color.gray)
                                }
                                else {
                                    RoundedRectangle(cornerRadius: 10).fill(Color.blue)
                                }
                            }
                        )
                        .mask(RoundedRectangle(cornerSize: CGSize(width: 10, height: 10)))
                }
            )
            .scaleEffect(configuration.isPressed ? 0.95 : 1)
            //.animation(.spring())
    }
}

struct NeumorphicButtonCircleStyle: ButtonStyle {
    
    var bgColor: String
    var borderColor: String
    var size: CGFloat
    
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .padding(10)
            .contentShape(Circle())
            .background(
                Group {
                    Circle()
                        .strokeBorder(Color(borderColor), lineWidth: 2)
                        .frame(width: size, height: size)
                        .overlay(
                            Circle()
                                .stroke(Color.white.opacity(0.16), lineWidth: 8)
                                .blur(radius: 2)
                                .frame(width: size + 10, height: size + 10)
                                .offset(x: 4, y: 4)
                        )
                        .overlay(
                            Circle()
                                .stroke(Color.black.opacity(0.50), lineWidth: 8)
                                .blur(radius: 2)
                                .frame(width: size + 10, height: size + 10)
                                .offset(x: -4, y: -4)
                        )
                        .background(Color(bgColor))
                        .mask(Circle())
                }
            )
            .scaleEffect(configuration.isPressed ? 0.95 : 1)
            //.animation(.spring())
    }
}

struct ClockView: View {
    
    @ObservedObject var timerManager: TimerManager
    var size: CGFloat
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(Color.gray.opacity(0.1), style: StrokeStyle(lineWidth: 10))
                .frame(width: size, height: size, alignment: .center)
                .rotationEffect(Angle(degrees: -90))
            
            if self.timerManager.timerMode != .finished {
                Circle()
                    .trim(from: 0.0, to: CGFloat(((self.timerManager.customTimer.nbOfMillisecond-self.timerManager.timeLeft)/self.timerManager.customTimer.nbOfMillisecond)+0.0001))
                    .stroke(Color(self.timerManager.customTimer.color!), style: StrokeStyle(lineWidth: 10, lineCap: .round))
                    .frame(width: size, height: size, alignment: .center)
                    .rotationEffect(Angle(degrees: -90))
            } else {
                Circle()
                    .trim(from: 0.0, to: 1.0)
                    .stroke(Color(self.timerManager.customTimer.color!), style: StrokeStyle(lineWidth: 10, lineCap: .round))
                    .frame(width: size, height: size, alignment: .center)
                    .rotationEffect(Angle(degrees: -90))
            }
        }
        .padding(.init(top: paddingClockSize, leading: 0, bottom: paddingClockSize, trailing: 0))
    }
}

struct ButtonRoundedRectangleStyle: ButtonStyle {
    
    var bgColor: String
    var timerColor: String
    var width: CGFloat
    var height: CGFloat
    
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .padding(0)
            .contentShape(RoundedRectangle(cornerSize: CGSize(width: 50, height: 50)))
            .background(
                Group {
                    RoundedRectangle(cornerSize: CGSize(width: 50, height: 50))
                        .strokeBorder(Color(self.timerColor), lineWidth: 2)
                        .frame(width: width, height: height)
                        .overlay(
                            RoundedRectangle(cornerSize: CGSize(width: 50, height: 50))
                                .stroke(Color.white.opacity(0.16), lineWidth: 8)
                                .blur(radius: 2)
                                .frame(width: width+10, height: height+10)
                                .offset(x: 4, y: 4)
                        )
                        .overlay(
                            RoundedRectangle(cornerSize: CGSize(width: 50, height: 50))
                                .stroke(Color.black.opacity(0.50), lineWidth: 8)
                                .blur(radius: 2)
                                .frame(width: width+10, height: height+10)
                                .offset(x: -4, y: -4)
                        )
                        /*.overlay(
                            Triangle()
                                .fill(Color.white.opacity(0.02))
                                .frame(width: width+10, height: height+10)
                        )*/
                        .background(Color(bgColor))
                        .mask(RoundedRectangle(cornerSize: CGSize(width: 50, height: 50)))
                    
                }
            )
            .scaleEffect(configuration.isPressed ? 0.95 : 1)
            //.animation(.spring())
    }
}

struct Triangle: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        path.move(to: CGPoint(x: rect.minX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        
        return path
    }
}

// OK
struct ButtonItemListStyle: ButtonStyle {
    
    var bgColor: String
    var timerColor: String
    var width: CGFloat
    var height: CGFloat
    
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .padding(0)
            .contentShape(RoundedRectangle(cornerSize: CGSize(width: 10, height: 10)))
            .background(
                Group {
                    RoundedRectangle(cornerSize: CGSize(width: 10, height: 10))
                        .strokeBorder(Color(self.timerColor), lineWidth: 2)
                        .frame(width: width, height: height)
                        .overlay(
                            RoundedRectangle(cornerSize: CGSize(width: 10, height: 10))
                                .stroke(Color.white.opacity(0.16), lineWidth: 8)
                                .blur(radius: 2)
                                .frame(width: width+10, height: height+10)
                                .offset(x: 4, y: 4)
                        )
                        .overlay(
                            RoundedRectangle(cornerSize: CGSize(width: 10, height: 10))
                                .stroke(Color.black.opacity(0.50), lineWidth: 8)
                                .blur(radius: 2)
                                .frame(width: width+10, height: height+10)
                                .offset(x: -4, y: -4)
                        )
                        .overlay(
                            RoundedRectangle(cornerSize: CGSize(width: 10, height: 10))
                                .fill(Color(self.timerColor))
                                .frame(width: width/3, height: 2)
                                .offset(x: 0, y: 7)
                        )
                        /*.overlay(
                            Triangle()
                                .fill(Color.white.opacity(0.02))
                                .frame(width: width+10, height: height+10)
                        )*/
                        .background(Color(bgColor))
                        .mask(RoundedRectangle(cornerSize: CGSize(width: 10, height: 10)))
                    
                }
            )
            .scaleEffect(1.05)
            .scaleEffect(configuration.isPressed ? 0.95 : 1)
            //.animation(.easeInOut)
    }
}

extension LinearGradient {
    init(_ colors: Color...) {
        self.init(gradient: Gradient(colors: colors), startPoint: .topLeading, endPoint: .bottomTrailing)
    }
    
    init(_ isVertical: Bool, _ colors: Color...) {
        if isVertical {
            self.init(gradient: Gradient(colors: colors), startPoint: .top, endPoint: .bottom)
        }
        else {
            self.init(gradient: Gradient(colors: colors), startPoint: .leading, endPoint: .trailing)
        }
    }
}

struct CustomPickerColor: View {
    var color: String
    
    var body: some View {
        Circle()
            .fill(Color(color))
            .frame(width: sizePickerColor, height: sizePickerColor, alignment: .center)
    }
}

struct CustomPicker : UIViewRepresentable {
    
    var data: Array<Int>
    
    @Binding var selectedItem: Int
    var color: String
    
    init(data: Array<Int>, selectedItem: Binding<Int>, color: String) {
        self.data = data
        self._selectedItem = selectedItem
        self.color = color
        
        UIPickerView.appearance().tintColor = UIColor(Color.blue)
        UISegmentedControl.appearance().selectedSegmentTintColor = UIColor(Color.blue)
    }
    
    func makeCoordinator() -> CustomPicker.Coordinator {
        return CustomPicker.Coordinator(parent1: self, color: color)
    }
    
    func makeUIView(context: UIViewRepresentableContext<CustomPicker>) -> UIPickerView {
        let picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        picker.dataSource = context.coordinator
        picker.delegate = context.coordinator
        picker.selectRow(selectedItem, inComponent: 0, animated: false)
        
        return picker
    }
    
    func updateUIView(_ uiView: UIPickerView, context: UIViewRepresentableContext<CustomPicker>) {
        var label: UILabel
        label = uiView.view(forRow: selectedItem, forComponent: 0)?.subviews[0] as! UILabel
        label.textColor = UIColor(Color("White"))
    }
    
    class Coordinator : NSObject, UIPickerViewDelegate, UIPickerViewDataSource {
        var parent: CustomPicker
        var color: String
        
        init(parent1: CustomPicker, color: String) {
            self.parent = parent1
            self.color = color
        }
        
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            
            return parent.data.count
        }
        
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        
        func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: pickerSize, height: pickerSize))
            view.tag = row
            
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerSize, height: pickerSize))
            
            if row == 0 {
                label.text = "00"
            }
            else if row < 10 {
                label.text = "0\(parent.data[row])"
            }
            else {
                label.text = String(parent.data[row])
            }
            
            label.textAlignment = .center
            label.textColor = parent.selectedItem == row ? UIColor(Color("White")) : UIColor(Color("Gray").opacity(0.5))
            label.font = UIFont(name: "SFProDisplay-Semibold", size: sizePickerLabel)
            
            view.backgroundColor = .clear
            view.addSubview(label)
            view.bringSubviewToFront(label)
            
            view.clipsToBounds = true
            
            return view
        }
        
        func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
            return pickerSize
        }
        
        func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
            return pickerSize
        }
        
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            self.parent.selectedItem = parent.data[row]
        }
    }
    
}

// Ajoute le swipe au custom back button
extension UINavigationController: UIGestureRecognizerDelegate {
    override open func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = self
    }
    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count > 1
    }
}

