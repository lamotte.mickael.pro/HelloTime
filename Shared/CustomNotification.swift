//
//  CustomNotification.swift
//  HelloTime
//
//  Created by Mickael Lamotte on 28/09/2021.
//

import Foundation
import SwiftUI
import UserNotifications

class LocalNotificationManager: ObservableObject {
    
    @Published var notification: [UNNotificationRequest] = []
    
    init() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in
            if granted == true && error == nil {
                print("Notification permitted !")
            }
            else {
                print("Notification denied !")
            }
        }
    }
    
    func sendNotification(title: String, subtitle: String?, body: String?, launchIn: Double) {
        
        let content = UNMutableNotificationContent()
        content.title = title
        if let subtitle = subtitle {
            content.subtitle = subtitle
        }
        if let body = body {
            content.body = body
        }
        
        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "alarm.wav"))
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: launchIn, repeats: false)
        let request = UNNotificationRequest(identifier: "demoNotification", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { requests in
            for request in requests {
                print(request)
            }
        })
    }
    
    func removeNotification() {
        let center = UNUserNotificationCenter.current()
        
        center.removeAllPendingNotificationRequests()
    }
}
