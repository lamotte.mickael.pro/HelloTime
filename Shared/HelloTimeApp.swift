//
//  HelloTimeApp.swift
//  Shared
//
//  Created by Mickael Lamotte on 09/09/2021.
//

import SwiftUI
import StoreKit

@main
struct HelloTimeApp: App {
    let persistenceContainer = PersistenceController.shared
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    let productIDs = [
        "com.growup.HelloTime.IAP.6PackTimer",
        "com.growup.HelloTime.IAP.PremiumTimer"
    ]
    
    @StateObject var storeManager = StoreManager()
    
    var body: some Scene {
        WindowGroup {
            MenuView(storeManager: storeManager)
                .environment(\.managedObjectContext, persistenceContainer.container.viewContext)
                .onAppear(perform: {
                    SKPaymentQueue.default().add(storeManager)
                    storeManager.getProducts(productIDs: productIDs)
                })
        }
    }
}
