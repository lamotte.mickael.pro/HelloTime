//
//  TimerManager.swift
//  HelloTime
//
//  Created by Mickael Lamotte on 20/09/2021.
//

import Foundation
import SwiftUI

class TimerManager: ObservableObject {
    
    @Published var timerMode: TimerMode = .initial
    
    @Published var hour: String = ""
    @Published var minute: String = ""
    @Published var second: String = ""
    @Published var millisecond: String = ""
    
    @Published var timeLeft: Double = 0
    
    var customTimer: CustomTimer
    var timer = Timer()
    
    var estimatedTimeOfEnd: Double = 0
    var timeOfPause: Double = 0
    
    init(timer: CustomTimer) {
        self.customTimer = timer
        self.timeLeft = self.customTimer.nbOfMillisecond
        self.initTimerString()
    }
    
    func resetTimerManager() {
        //self.customTimer = nil
        /*self.customTimer = CustomTimer()
        self.customTimer!.id = UUID()
        self.customTimer!.title = ""
        self.customTimer!.nbOfMillisecond = 0
        self.customTimer!.color = "MyColor1"
 */
        
        
        timer = Timer()
        estimatedTimeOfEnd = 0
        timeOfPause = 0
        
        self.timeLeft = 0
    }
    /*
    func initTimerManager(timer: CustomTimer) {
        self.customTimer = timer
        self.timeLeft = self.customTimer!.nbOfMillisecond
        self.initTimerString()
    }
 */
    
    func initTimerString() {
        self.hour = Int(customTimer.nbOfMillisecond / 360000) < 10
            ? "0\(String(Int(customTimer.nbOfMillisecond / 360000)))"
            : String(Int(customTimer.nbOfMillisecond / 360000))
        
        self.minute = Int(customTimer.nbOfMillisecond / 6000 ) % 60 < 10
            ? "0\(String(Int(customTimer.nbOfMillisecond / 6000 ) % 60))"
            : String(Int(customTimer.nbOfMillisecond / 6000 ) % 60)
        
        self.second = Int(customTimer.nbOfMillisecond / 100 ) % 60 < 10
            ? "0\(String(Int(customTimer.nbOfMillisecond / 100 ) % 60))"
            : String(Int(customTimer.nbOfMillisecond / 100 ) % 60)
        
        self.millisecond = Int(customTimer.nbOfMillisecond) % 100 < 10
            ? "0\(String(Int(customTimer.nbOfMillisecond) % 100))"
            : String(Int(customTimer.nbOfMillisecond) % 100)
    }
    
    func updateStringTimer() {
        self.hour = Int(self.timeLeft / 360000) < 10
            ? "0\(String(Int(self.timeLeft / 360000)))"
            : String(Int(self.timeLeft / 360000))
        
        self.minute = Int(self.timeLeft / 6000 ) % 60 < 10
            ? "0\(String(Int(self.timeLeft / 6000 ) % 60))"
            : String(Int(self.timeLeft / 6000 ) % 60)
        
        self.second = Int(self.timeLeft / 100 ) % 60 < 10
            ? "0\(String(Int(self.timeLeft / 100 ) % 60))"
            : String(Int(self.timeLeft / 100 ) % 60)
        
        self.millisecond = Int(self.timeLeft) % 100 < 10
            ? "0\(String(Int(self.timeLeft) % 100))"
            : String(Int(self.timeLeft) % 100)
    }
    
    func start() {
        self.timerMode = .running
        self.estimatedTimeOfEnd = Date().timeIntervalSince1970 * 100 + self.customTimer.nbOfMillisecond
        
        timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true, block: { timer in
            if  Date().timeIntervalSince1970 * 100 > self.estimatedTimeOfEnd {
                timer.invalidate()
                self.timerMode = .finished
                self.estimatedTimeOfEnd = 0
                self.timeLeft = self.customTimer.nbOfMillisecond
                self.initTimerString()
                playSound(sound: "alarm", type: "wav")
            }
            else {
                self.timeLeft = self.estimatedTimeOfEnd - (Date().timeIntervalSince1970 * 100)
                self.updateStringTimer()
            }
        })
    }
    
    func pause() {
        self.timerMode = .paused
        self.timeOfPause = Date().timeIntervalSince1970 * 100
        timer.invalidate()
    }
    
    func playAfterPause() {
        self.timerMode = .running
        self.estimatedTimeOfEnd = self.estimatedTimeOfEnd + (Date().timeIntervalSince1970 * 100 - self.timeOfPause)
        self.timeLeft = self.estimatedTimeOfEnd - (Date().timeIntervalSince1970 * 100)
        timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true, block: { timer in
            if  Date().timeIntervalSince1970 * 100 > self.estimatedTimeOfEnd {
                timer.invalidate()
                self.timerMode = .finished
                self.estimatedTimeOfEnd = 0
                self.timeLeft = self.customTimer.nbOfMillisecond
                self.initTimerString()
            }
            else {
                self.timeLeft = self.estimatedTimeOfEnd - (Date().timeIntervalSince1970 * 100)
                self.updateStringTimer()
            }
        })
    }
    
    func stop() {
        timer.invalidate()
        self.timerMode = .initial
        self.estimatedTimeOfEnd = 0
        self.timeLeft = self.customTimer.nbOfMillisecond
        self.initTimerString()
    }
    
    func finish() {
        self.timerMode = .initial
    }
}
