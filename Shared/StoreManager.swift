//
//  StoreManager.swift
//  HelloTime
//
//  Created by Mickael Lamotte on 15/11/2021.
//

import Foundation
import StoreKit

class StoreManager: NSObject, ObservableObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    @Published var myProducts = [SKProduct]()
    @Published var transactionState: SKPaymentTransactionState?
    
    var request: SKProductsRequest!
    
    private var completedPurshases = [String]() {
        didSet {
            var count6Pack = 0
            for purshase in completedPurshases {
                if purshase == "com.growup.HelloTime.IAP.PremiumTimer" {
                    UserDefaults.standard.setValue(true, forKey: purshase)
                }
                else if purshase == "com.growup.HelloTime.IAP.6PackTimer" {
                    count6Pack += 1
                }
            }
            UserDefaults.standard.setValue((count6Pack+1) * 6, forKey: "maxNumberOfTimer")
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("Did receive response")
        
        if !response.products.isEmpty {
            for fetchedProduct in response.products {
                DispatchQueue.main.async {
                    self.myProducts.append(fetchedProduct)
                }
            }
        }
        
        for invalidIdentifier in response.invalidProductIdentifiers {
            print("Invalid identifiers found: \(invalidIdentifier)")
        }
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Invalid identifiers found: \(error)")
    }
    
    func getProducts(productIDs: [String]) {
        print("Starts requesting products ...")
        let request = SKProductsRequest(productIdentifiers: Set(productIDs))
        request.delegate = self
        request.start()
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchasing:
                transactionState = .purchasing
                print("Purshasing")
            case .purchased:
                completedPurshases.append(transaction.payment.productIdentifier)
                queue.finishTransaction(transaction)
                transactionState = .purchased
                print("Purshased")
            case .restored:
                completedPurshases.append(transaction.payment.productIdentifier)
                queue.finishTransaction(transaction)
                transactionState = .restored
                print("Restored")
            case .failed, .deferred:
                print("Payment Queue Error: \(String(describing: transaction.error))")
                queue.finishTransaction(transaction)
                transactionState = .failed
            default:
                queue.finishTransaction(transaction)
            }
        }
    }
    
    func purshaseProduct(product: SKProduct) {
        if SKPaymentQueue.canMakePayments() {
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(payment)
        } else {
            print("User can't make payment.")
        }
    }
    
    func restoreProducts() {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
}

extension SKProduct {
    
    var localizedPrice: String? {
        let numberFormatter = NumberFormatter()
        
        numberFormatter.locale = priceLocale
        numberFormatter.numberStyle = .currency
        
        return numberFormatter.string(from: price)
    }
}
