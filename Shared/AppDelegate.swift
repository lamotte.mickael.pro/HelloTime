//
//  AppDelegate.swift
//  HelloTime
//
//  Created by Mickael Lamotte on 05/10/2021.
//

import Foundation
import UIKit

class AppDelegate: NSObject, UIApplicationDelegate {
    
    static var orientationLock = UIInterfaceOrientationMask.portrait
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return AppDelegate.orientationLock
    }
    
    func changeOrientationLock() {
        if AppDelegate.orientationLock == UIInterfaceOrientationMask.portrait {
            AppDelegate.orientationLock = UIInterfaceOrientationMask.landscape
        } else {
            AppDelegate.orientationLock = UIInterfaceOrientationMask.portrait
        }
    }
}
