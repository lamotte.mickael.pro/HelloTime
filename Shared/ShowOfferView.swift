//
//  ShowOffer.swift
//  HelloTime
//
//  Created by Mickael Lamotte on 09/11/2021.
//

import SwiftUI
import StoreKit

struct ShowOfferView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @ObservedObject var storeManager: StoreManager
    
    var offerFree = ["Limited number of timer : 6".localized(), "No Ads".localized()]
    var offer6Pack = ["Limited number of timer increased by 6".localized(), "No Ads".localized()]
    var offerPremium = ["Infinit number of timer".localized(), "No Ads".localized()]
    
    var isAquiredPremium = UserDefaults.standard.bool(forKey: "com.growup.HelloTime.IAP.PremiumTimer")
    
    var body: some View {
        GeometryReader { geometry in
                ZStack {
                    Color("BackgroundColor").edgesIgnoringSafeArea(.all)
                    
                    ScrollView {
                        VStack {
                            
                            VStack {
                                Offer(title: "Free".localized(), offer: offerFree, isAcquired: true, storeManager: storeManager, width: geometry.size.width/2, height: 50)
                                //Offer(title: "Free".localized(), offer: offerFree, isAcquired: true, width: geometry.size.width/2, height: 50)
                            }
                            .padding(.bottom, 10)
                            
                            Rectangle()
                                .frame(width: geometry.size.width - 40, height: 1, alignment: .center)
                                .foregroundColor(Color.white.opacity(0.1))
                            
                            VStack {
                                Offer(title: "6 Pack".localized(), offer: offer6Pack, isAcquired: isAquiredPremium, product: storeManager.myProducts[0], storeManager: storeManager, width: geometry.size.width/2, height: 50)
                                //Offer(title: "6 Pack".localized(), offer: offer6Pack, isAcquired: isAquiredPremium, width: geometry.size.width/2, height: 50)
                            }
                            .padding(.vertical, 10)
                            
                            Rectangle()
                                .frame(width: geometry.size.width - 40, height: 1, alignment: .center)
                                .foregroundColor(Color.white.opacity(0.1))
                            
                            VStack {
                                Offer(title: "Premium", offer: offerPremium, isAcquired: isAquiredPremium, product: storeManager.myProducts[1], storeManager: storeManager, width: geometry.size.width/2, height: 50)
                                //Offer(title: "Premium", offer: offerPremium, isAcquired: isAquiredPremium, width: geometry.size.width/2, height: 50)
                            }
                            .padding(.vertical, 10)
                            
                            Spacer()
                        }
                        .padding(.top, 20)
                    }
                }
                .navigationBarItems(trailing:
                    HStack {
                        Button(action: {
                            storeManager.restoreProducts()
                        }) {
                            Text("Restore Purchases".localized())
                        }
                    }
                )
                .navigationTitle("Current offers".localized())
                .onAppear(perform: {
                    print(UserDefaults.standard.integer(forKey: "maxNumberOfTimer"))
                    UIScrollView.appearance().bounces = true
                })
        }
    }
}

struct Offer: View {
    
    var title: String
    var offer: [String]
    var isAcquired: Bool
    var product: SKProduct?
    
    @ObservedObject var storeManager: StoreManager
    
    var width: CGFloat
    var height: CGFloat
    
    var body: some View {
        
        HStack {
            Text(self.title)
                .font(Font.custom("SFProDisplay-Bold", size: 26))
                .foregroundColor(Color.white)
                .padding(.leading, 20)
            Spacer()
        }
        
        HStack {
            Text("Description of the offer".localized())
                .font(Font.custom("SFProDisplay-Semibold", size: 20))
                .foregroundColor(Color.white.opacity(0.7))
            Spacer()
        }
        .padding(.leading, 20)
        
        HStack {
            VStack(alignment: .leading) {
                Label(offer[0], systemImage: "circlebadge.fill")
                    .font(Font.custom("SFProDisplay-Semibold", size: 14))
                    .foregroundColor(Color.white)
                    .padding(.bottom, 3)
                Label(offer[1], systemImage: "circlebadge.fill")
                    .font(Font.custom("SFProDisplay-Semibold", size: 14))
                    .foregroundColor(Color.white)
            }
            .padding(.vertical, 5)
            Spacer()
        }
        .padding(.leading, 20)
        
        Button(action: {
            if product != nil {
                storeManager.purshaseProduct(product: product!)
            }
        }, label: {
            if isAcquired {
                Text("Purshased".localized().uppercased())
                    .font(Font.custom("SFProDisplay-Semibold", size: 16))
                    .foregroundColor(Color.white)
            } else {
                Text("Buy".localized() + "   \(self.product!.localizedPrice!)".uppercased())
                    .font(Font.custom("SFProDisplay-Semibold", size: 16))
                    .foregroundColor(Color.white)
                /*Text("Buy".localized() + "   1.99€".uppercased())
                    .font(Font.custom("SFProDisplay-Semibold", size: 16))
                    .foregroundColor(Color.white)*/
            }
        })
        .buttonStyle(ButtonOfferPremium(isAcquired: isAcquired, width: width, height: height))
        .frame(width: width, height: height, alignment: .center)
        .disabled(isAcquired)
    }
}

struct ShowOfferView_Previews: PreviewProvider {
    static var previews: some View {
        ShowOfferView(storeManager: StoreManager())
    }
}
