//
//  MenuView.swift
//  HelloTime
//
//  Created by Mickael Lamotte on 09/09/2021.
//

import SwiftUI
import CoreData

struct MenuView: View {
    
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest(sortDescriptors: [NSSortDescriptor(keyPath: \CustomTimer.title, ascending: true)]) var timers: FetchedResults<CustomTimer>
    
    @AppStorage("maxNumberOfTimer") var maxNumberOfTimer = 6
    @AppStorage("com.growup.HelloTime.IAP.PremiumTimer") var isPremium = false
    
    @StateObject var notificationManager = LocalNotificationManager()
    
    @StateObject var storeManager: StoreManager
    
    @State var updateMode: Bool = false
    @State var actualTimer: CustomTimer?
    
    @State var showingAlert = false
    
    let layout = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    init(storeManager: StoreManager) {
        self._storeManager = StateObject(wrappedValue: storeManager)
        
        if UserDefaults.standard.integer(forKey: "maxNumberOfTimer") == 0 {
            UserDefaults.standard.setValue(6, forKey: "maxNumberOfTimer")
        }
    }
    
    var body: some View {
        GeometryReader { geometry in
            NavigationView {
                ZStack {
                    Color("BackgroundColor").edgesIgnoringSafeArea(.all)
                    ZStack {
                        ScrollView(.vertical, showsIndicators: false) {
                            LazyVGrid(columns: layout, alignment: .center, spacing: 5) {
                                ForEach(timers, id: \.id) { timer in
                                    if !self.updateMode {
                                        NavigationLink(destination: TimerView(timer: timer, notificationManager: notificationManager)) {
                                            itemMenu(name: timer.title!, color: timer.color ?? "BackgroundColor", time: initTimerString(nbOfMillisecond: timer.nbOfMillisecond), width: (geometry.size.width/2) - 25, height: ((geometry.size.width/2) - 25)/2)
                                        }
                                        .buttonStyle(ButtonItemListStyle(bgColor: "BackgroundColor", timerColor: timer.color ?? "BackgroundColor", width: geometry.size.width/2 - 25, height: ((geometry.size.width/2) - 25)/2))
                                        .frame(width: (geometry.size.width/2) - 25, height: ((geometry.size.width/2) - 25)/2, alignment: .center)
                                        .padding(.bottom, 10)
                                    } else {
                                        ZStack {
                                            NavigationLink(destination: AddTimerView(timer: timer, timersCount: timers.count)) {
                                                itemMenu(name: timer.title!, color: timer.color ?? "BackgroundColor", time: initTimerString(nbOfMillisecond: timer.nbOfMillisecond), width: (geometry.size.width/2) - 25, height: ((geometry.size.width/2) - 25)/2)
                                            }
                                            .buttonStyle(ButtonItemListStyle(bgColor: "BackgroundColor", timerColor: timer.color ?? "BackgroundColor", width: geometry.size.width/2 - 25, height: ((geometry.size.width/2) - 25)/2))
                                            .frame(width: geometry.size.width/2 - 25, height: ((geometry.size.width/2) - 25)/2, alignment: .center)
                                            .padding(.bottom, 10)
                                            
                                            Button(action: {
                                                actualTimer = timer
                                                showingAlert = true
                                            }, label: {
                                                LogoDelete()
                                            })
                                            .offset(x: ((geometry.size.width/2) - 25)/2 - 5, y: (((geometry.size.width/2) - 25)/2)/2 - ((geometry.size.width/2) - 25)/2)
                                            .alert(isPresented: self.$showingAlert) {
                                                Alert(
                                                    title: Text("Remove".localized() + " \"\(actualTimer!.title!)\" ?"),
                                                    message: Text("The timer will be permanently deleted.".localized()),
                                                    primaryButton: .destructive(Text("Delete".localized())) { deleteTimer(timer: actualTimer!)
                                                        showingAlert = false
                                                    },
                                                    secondaryButton: .cancel(Text("Cancel".localized()))
                                                )
                                            }
                                        }
                                    }
                                }
                            }
                            .padding(10)
                            .padding(.top, 10)
                            .padding(.bottom, 40)
                            
                            if !self.updateMode {
                                if timers.count < maxNumberOfTimer || isPremium {
                                    VStack {
                                        Text("Push the \"+\" button".localized())
                                            .font(Font.custom("SFProDisplay-Medium", size: messageMenuSize))
                                            .foregroundColor(Color.white.opacity(0.8))
                                        Text("to add a new timer in your list".localized())
                                            .font(Font.custom("SFProDisplay-Medium", size: messageMenuSize-8))
                                            .foregroundColor(Color.white.opacity(0.6))
                                            .multilineTextAlignment(.center)
                                            .frame(width: 140)
                                    }
                                    .padding(.bottom, 180)
                                } else {
                                    VStack {
                                        Text("You have reached the timer limit !".localized())
                                            .font(Font.custom("SFProDisplay-Medium", size: messageMenuSize))
                                            .foregroundColor(Color.white.opacity(0.8))
                                            .multilineTextAlignment(.center)
                                            .frame(width: 300)
                                        Text("To increase the number of timers, please subscribe to one of the premium offers.".localized())
                                            .font(Font.custom("SFProDisplay-Medium", size: messageMenuSize-8))
                                            .foregroundColor(Color.white.opacity(0.6))
                                            .multilineTextAlignment(.center)
                                            .frame(width: 260)
                                    }
                                    .padding(.bottom, 180)
                                }
                            }
                        }
                        
                        if !self.updateMode {
                            VStack {
                                Spacer()
                            
                                HStack {
                                    if timers.count < maxNumberOfTimer || isPremium {
                                        NavigationLink(destination: AddTimerView(timersCount: timers.count)) {
                                            Image(systemName: "plus")
                                                .resizable(resizingMode: .stretch)
                                                .frame(width: plusSize, height: plusSize, alignment: .center)
                                                .foregroundColor(.white)
                                        }
                                        .buttonStyle(NeumorphicButtonCircleStyle(bgColor: "BackgroundColor", borderColor: "White", size: plusSize*2))
                                        .padding(.init(top: 20, leading: 0, bottom: bottomPaddingButtonMenuView, trailing: 0))
                                    } else {
                                        NavigationLink(destination: ShowOfferView(storeManager: storeManager)) {
                                            Text("Upgrade to Premium")
                                                .font(Font.custom("SFProDisplay-Medium", size: 20))
                                                .padding(5)
                                                .foregroundColor(.white)
                                                .frame(width: 300, height: 20, alignment: .center)
                                        }
                                        .buttonStyle(ButtonRoundedRectangleStyle(bgColor: "BackgroundColor", timerColor: "White", width: 300, height: 80))
                                        .padding(.init(top: 20, leading: 0, bottom: bottomPaddingButtonMenuView, trailing: 0))
                                    }
                                }
                                .frame(maxWidth: .infinity, maxHeight: 140, alignment: .center)
                                .background(LinearGradient(true, Color("BackgroundColor").opacity(0),
                                    Color("BackgroundColor"),
                                    Color("BackgroundColor"),
                                    Color("BackgroundColor"),
                                    Color("BackgroundColorNavigationBar")))
                            }
                            .edgesIgnoringSafeArea(.bottom)
                        }
                    }
                }
                .navigationBarItems(trailing:
                    HStack {
                        if !self.updateMode {
                            Button(action: {
                                self.updateMode = true
                            }, label: {
                                Text("Edit".localized())
                            })
                        } else {
                            Button(action: {
                                self.updateMode = false
                            }, label: {
                                Text("Cancel".localized())
                            })
                        }
                    }
                )
                .navigationTitle("My Timers".localized())
                .onAppear(perform: {
                    if !UserDefaults.standard.bool(forKey: "isNewUser") {
                        addTimer(title: "Cooking Chicken".localized(), nbOfMillisecond: 600000, color: "MyColor9")
                        addTimer(title: "Push-Ups".localized(), nbOfMillisecond: 9000, color: "MyColor6")
                        addTimer(title: "Rest".localized(), nbOfMillisecond: 120000, color: "MyColor8")
                        UserDefaults.standard.setValue(true, forKey: "isNewUser")
                    }
                    
                    let navBarBackgroundColor = UINavigationBarAppearance()
                    navBarBackgroundColor.backgroundColor = UIColor(Color("BackgroundColorNavigationBar"))
                    navBarBackgroundColor.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
                    navBarBackgroundColor.titleTextAttributes = [.foregroundColor: UIColor.white]
                    
                    UINavigationBar.appearance().standardAppearance = navBarBackgroundColor
                    UINavigationBar.appearance().scrollEdgeAppearance = navBarBackgroundColor
                    
                    UIScrollView.appearance().bounces = true
                })
                .onTapGesture {
                    if self.updateMode {
                        self.updateMode = false
                    }
                }
            }
        }
    }
    
    private func deleteTimer(timer: CustomTimer) {
        viewContext.delete(timer)
        saveContext()
    }
    
    private func saveContext() {
        do {
            try viewContext.save()
        } catch {
            let error = error as NSError
            fatalError("Unresolved error : \(error)")
        }
    }
    
    private func addTimer(title: String, nbOfMillisecond: Double, color: String) {
        let newTimer = CustomTimer(context: viewContext)
        newTimer.id = UUID()
        newTimer.title = title
        newTimer.nbOfMillisecond = nbOfMillisecond
        newTimer.color = color
        
        saveContext()
    }
}

struct itemMenu: View {
    var name: String
    var color: String
    var time: String
    var width: CGFloat
    var height: CGFloat
    
    var body: some View {
        VStack {
            if name.count < 12 {
                Text(name)
                    .font(Font.custom("SFProDisplay-Medium", size: titleItemMenuSize))
                    .foregroundColor(Color.white)
                    .offset(x: 0, y: -5)
                    .lineLimit(1)
                
                Text(time)
                    .font(Font.custom("SFProDisplay-Semibold", size: subtitleItemMenuSize))
                    .foregroundColor(Color.white)
                    .offset(x: 0, y: offsetSubtitleItemMenu)
            } else {
                Text(name)
                    .font(Font.custom("SFProDisplay-Medium", size: titleItemMenuSize-8))
                    .foregroundColor(Color.white)
                    .offset(x: 0, y: -4)
                    .lineLimit(1)
                
                Text(time)
                    .font(Font.custom("SFProDisplay-Semibold", size: subtitleItemMenuSize))
                    .foregroundColor(Color.white)
                    .offset(x: 0, y: offsetSubtitleItemMenu+5)
            }
        }
        .frame(width: width, height: height)
    }
}

struct LogoDelete: View {
    var body: some View {
        ZStack {
            Circle()
                .fill(Color("Gray"))
                .frame(width: logoDeleteSize, height: logoDeleteSize, alignment: .center)
            
            RoundedRectangle(cornerRadius: 50)
                .fill(Color("BackgroundColor"))
                .frame(width: logoDeleteSize/2, height: logoDeleteMinusSize, alignment: .center)
        }
    }
}

struct addItemMenu: View {
    var body: some View {
        Text("+")
            .fontWeight(.bold)
            .font(.title)
            .foregroundColor(.white)
            .frame(maxWidth: .infinity, minHeight: 50, maxHeight: 50)
            .background(Color("BackgroundColor"))
    }
}

/*
struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
            MenuView(storeManager: StoreManager())
    }
}
*/
