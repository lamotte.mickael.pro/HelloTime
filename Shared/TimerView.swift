//
//  Timer.swift
//  HelloTime
//
//  Created by Mickael Lamotte on 09/09/2021.
//

import SwiftUI
import CoreData

struct TimerView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var notificationManager: LocalNotificationManager
    
    @ObservedObject var timerManager: TimerManager
    
    @FetchRequest(sortDescriptors: [NSSortDescriptor(keyPath: \CustomTimer.title, ascending: true)]) var timers: FetchedResults<CustomTimer>
    
    let timer: CustomTimer
    
    @State var color: String = "MyColor1"
    
    @State var isEditModeOn = false
    
    @State var title: String = ""
    
    init(timer: CustomTimer, notificationManager: LocalNotificationManager) {
        self.timer = timer
        self.timerManager = TimerManager(timer: timer)
        self.notificationManager = notificationManager
        
        self._color = State(wrappedValue: timer.color!)
        self._title = State(wrappedValue: timer.title!)
    }
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                
                Color("BackgroundColor").edgesIgnoringSafeArea(.all)
                    
                VStack {
                    HStack {
                        BackButtonWithTimeManager(timerManager: timerManager, notificationManager: notificationManager)
                            .padding(.top, 15)
                        Spacer()
                        if self.timerManager.timerMode != .finished {
                            VStack {
                                NavigationLink(destination: AddTimerView(timer: self.timer, timersCount: timers.count)) {
                                    Image(systemName: "gearshape")
                                        .resizable()
                                        .scaledToFit()
                                        .frame(width: navBarSize+10, height: navBarSize+10, alignment: .center)
                                        .foregroundColor(Color(self.timerManager.customTimer.color ?? self.color))
                                }
                                .padding(.init(top: 15, leading: 0, bottom: 0, trailing: 25))
                            }
                        }
                    }
                    .frame(height: 45, alignment: .center)
                    
                    VStack {
                        
                        Text(timerManager.customTimer.title!)
                            .font(.custom("SFProDisplay-Semibold", size: titleSize))
                            .lineLimit(1)
                            .foregroundColor(Color(self.timerManager.customTimer.color ?? self.color))
                            .padding(.init(top: paddingTopTitleSize, leading: 10, bottom: 0, trailing: 10))
                        
                        ZStack {
                            if self.timerManager.timerMode != .finished {
                                HStack {
                                    Text(timerManager.hour)
                                    Text(":")
                                    Text(timerManager.minute)
                                    Text(":")
                                    Text(timerManager.second)
                                }
                                .font(.custom("SFProDisplay-Semibold", size: labelTimerSize))
                                .foregroundColor(.white)
                                .frame(height: 40, alignment: .center)
                                
                                Text(timerManager.millisecond)
                                    .font(.custom("SFProDisplay-Semibold", size: labelMillisTimerSize))
                                    .foregroundColor(.white.opacity(0.4))
                                    .frame(height: 40, alignment: .center)
                                    .offset(x: 0, y: 40)
                                
                                ClockView(timerManager: timerManager, size: geometry.size.width * 0.75)
                            } else {
                                HStack {
                                    Text("Finished !".localized())
                                }
                                .font(.custom("SFProDisplay-Semibold", size: labelTimerSize))
                                .foregroundColor(.white)
                                .frame(height: 40, alignment: .center)
                                
                                ClockView(timerManager: timerManager, size: geometry.size.width * 0.75)
                            }
                        }
                        
                        VStack {
                            if timerManager.timerMode != .finished {
                                Button(action: {
                                    if timerManager.timerMode == .running {
                                        self.timerManager.pause()
                                        self.notificationManager.removeNotification()
                                    }
                                    else if timerManager.timerMode == .paused {
                                        self.timerManager.playAfterPause()
                                        self.notificationManager.sendNotification(title: "Timer finished !".localized(), subtitle: nil, body: nil, launchIn: self.timerManager.timeLeft/100)
                                    }
                                    else {
                                        self.timerManager.start()
                                        self.notificationManager.sendNotification(title: "Timer finished !".localized(), subtitle: nil, body: nil, launchIn: self.timerManager.customTimer.nbOfMillisecond/100)
                                    }
                                }, label: {
                                    Image(systemName: timerManager.timerMode == .running ? "pause.fill" : "play.fill")
                                        .resizable(resizingMode: .stretch)
                                        .frame(width: buttonPlayTimerSize/3, height: buttonPlayTimerSize/3, alignment: .center)
                                        .foregroundColor(Color(self.timerManager.customTimer.color ?? self.color))
                                })
                                .buttonStyle(NeumorphicButtonCircleStyle(bgColor: "BackgroundColor", borderColor: self.timerManager.customTimer.color ?? self.color, size: buttonPlayTimerSize))
                                .padding(.init(top: paddingButtonPlaySize, leading: 0, bottom: buttonPlayTimerSize/2, trailing: 0))
                                
                                Button(action: {
                                    self.timerManager.stop()
                                    self.notificationManager.removeNotification()
                                }, label: {
                                    Image(systemName: "stop.fill")
                                        .resizable(resizingMode: .stretch)
                                        .frame(width: buttonStopTimerSize/4, height: buttonStopTimerSize/4, alignment: .center)
                                        .foregroundColor(Color(self.timerManager.customTimer.color ?? self.color))
                                })
                                .buttonStyle(NeumorphicButtonCircleStyle(bgColor: "BackgroundColor", borderColor: self.timerManager.customTimer.color ?? self.color, size: buttonStopTimerSize))
                            } else {
                                Button(action: {
                                    self.timerManager.stop()
                                    self.notificationManager.removeNotification()
                                    audioPlayer?.stop()
                                }, label: {
                                    Image(systemName: "gobackward")
                                        .resizable(resizingMode: .stretch)
                                        .frame(width: buttonPlayTimerSize/3, height: buttonPlayTimerSize/3, alignment: .center)
                                        .foregroundColor(Color(self.timerManager.customTimer.color ?? self.color))
                                })
                                .buttonStyle(NeumorphicButtonCircleStyle(bgColor: "BackgroundColor", borderColor: self.timerManager.customTimer.color ?? self.color, size: buttonPlayTimerSize))
                                .padding(.init(top: paddingButtonPlaySize, leading: 0, bottom: buttonPlayTimerSize/2, trailing: 0))
                                
                                Button(action: {
                                    self.timerManager.finish()
                                    self.presentationMode.wrappedValue.dismiss()
                                    audioPlayer?.stop()
                                }, label: {
                                    Text("Back to home".localized())
                                        .font(.title2)
                                        .foregroundColor(.white)
                                })
                                .buttonStyle(ButtonRoundedRectangleStyle(bgColor: "BackgroundColor", timerColor: self.timerManager.customTimer.color ?? self.color, width: buttonWidthSize, height: buttonHeightSize))
                                .padding(.top, UIScreen.screenHeight > 740 ? 20 : 0)
                                
                                Spacer()
                            }
                        }
                        .padding(.top, UIScreen.screenHeight > 740 ? buttonPlayTimerSize/3 : 5) // VStack
                    } // VStack
                    Spacer()
                } // VStack
                
            } // ZStack
            .navigationBarHidden(true)
            .onAppear(perform: {
                self.timerManager.stop()
                self.notificationManager.removeNotification()
            })
            .onTapGesture {
                if self.timerManager.timerMode == .finished {
                    audioPlayer?.stop()
                }
            }
        }
    } // body
    
    private func editMode() {
        self.isEditModeOn = !self.isEditModeOn
    }
}
