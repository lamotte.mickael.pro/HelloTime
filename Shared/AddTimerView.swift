//
//  FormTimer.swift
//  HelloTime
//
//  Created by Mickael Lamotte on 13/09/2021.
//

import SwiftUI
import CoreData

struct AddTimerView: View {
    
    @Environment(\.managedObjectContext) private var viewContext
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @StateObject var notificationManager = LocalNotificationManager()
    @FetchRequest(sortDescriptors: [])
    var timers: FetchedResults<CustomTimer>
    var timer: CustomTimer?
    
    let firstLineColors = ["MyColor1",
                       "MyColor2",
                       "MyColor3",
                       "MyColor4",
                       "MyColor5"]
    
    let secondLineColor = ["MyColor6",
                       "MyColor7",
                       "MyColor8",
                       "MyColor9",
                       "MyColor10"]
    
    @State var isEditMode = false
    
    @State var timersCount: Int
    
    @StateObject var title = TextLimiter()
    
    @State var nbOfMillisecond: Double = 0
    
    @State var color = "MyColor\(Int.random(in: 1...10))"
    
    @State var selectedPickerHour = 0
    @State var selectedPickerMinute = 0
    @State var selectedPickerSecond = 15
    
    let hour = Array(0 ... 23)
    let minute = Array(0 ... 59)
    let second = Array(0 ... 59)
    
    @State var isSaveDisabled = false
    @State private var textFieldId: String = UUID().uuidString
    
    init(timer: CustomTimer, timersCount: Int) {
        self.timer = timer
        self._timersCount = State(wrappedValue: timersCount)
        
        self._nbOfMillisecond = State(wrappedValue: timer.nbOfMillisecond)
        self._color = State(wrappedValue: timer.color ?? "MyColor\(Int.random(in: 1...10))")
        self._isEditMode = State(wrappedValue: true)
        
        self._selectedPickerHour = State(wrappedValue: calculMillisecondToHour())
        self._selectedPickerMinute = State(wrappedValue: calculMillisecondToMinutes())
        self._selectedPickerSecond = State(wrappedValue: calculMillisecondToSecond())
    }
    
    init(timersCount: Int) {
        self._timersCount = State(wrappedValue: timersCount)
        self._isEditMode = State(wrappedValue: false)
    }
    
    var body: some View {
        
        GeometryReader { geometry in
            ZStack {
                
                Color("BackgroundColor").edgesIgnoringSafeArea(.all)
                
                ScrollView(.vertical, showsIndicators: false) {
                    VStack {
                        
                        HStack {
                            BackButton(color: "White")
                                .padding(.top, 15)
                            Spacer()
                        }
                        
                        ZStack {
                            if title.value.isEmpty {
                                Text("Name of the Timer".localized())
                                    .font(.custom("SFProDisplay-Semibold", size: titleTextFieldSize))
                                    .frame(width: abs(geometry.size.width - 30), height: heightTextField, alignment: .center)
                                    .foregroundColor(.white.opacity(0.4))
                                    .padding(.top, paddingTopTitle)
                                    .padding(.bottom, paddingBottomTitle)
                            }
                            TextField("", text: $title.value)
                                .id(textFieldId)
                                .font(.custom("SFProDisplay-Semibold", size: titleTextFieldSize))
                                .frame(width: abs(geometry.size.width - 30), height: heightTextField+5, alignment: .center)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 50)
                                        .strokeBorder(Color(color), lineWidth: 2)
                                        .frame(width: abs(geometry.size.width - 30), height: heightTextField+5, alignment: .center)
                                        .overlay(
                                            RoundedRectangle(cornerSize: CGSize(width: 50, height: 50))
                                                .stroke(Color.white.opacity(0.16), lineWidth: 8)
                                                .blur(radius: 2)
                                                .frame(width: abs(geometry.size.width - 30 + 10), height: heightTextField+15)
                                                .offset(x: 4, y: 4)
                                        )
                                        .overlay(
                                            RoundedRectangle(cornerSize: CGSize(width: 50, height: 50))
                                                .stroke(Color.black.opacity(0.50), lineWidth: 8)
                                                .blur(radius: 2)
                                                .frame(width: abs(geometry.size.width - 30 + 10), height: heightTextField+15)
                                                .offset(x: -4, y: -4)
                                        )
                                )
                                .mask(RoundedRectangle(cornerSize: CGSize(width: 50, height: 50)))
                                .foregroundColor(Color.white)
                                .multilineTextAlignment(.center)
                                .onTapGesture {}
                                .lineLimit(1)
                                .padding(.top, paddingTopTitle)
                                .padding(.bottom, paddingBottomTitle)
                            
                        }
                        
                        HStack {
                            ZStack {
                                CustomPicker(data: hour, selectedItem: self.$selectedPickerHour, color: "White")
                                    .frame(width: geometry.size.width/4.5, height: (geometry.size.width/4.5)*2, alignment: .center)
                                    .clipped()
                                    .compositingGroup()
                                    .clipShape(RoundedRectangle(cornerRadius: 15))
                                    .onChange(of: self.selectedPickerHour, perform: { value in
                                        if isTotalOfMillisEqualZero() {
                                            changeSaveDisabledTo(value: true)
                                        }
                                        else {
                                            changeSaveDisabledTo(value: false)
                                        }
                                    })
                                    
                                
                                RoundedRectangle(cornerRadius: 15)
                                    .stroke(Color(color), lineWidth: 8)
                                    .frame(width: geometry.size.width/4.5 + 10, height: geometry.size.width/4.5 + 10, alignment: .center)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color.white.opacity(0.16), lineWidth: 8)
                                            .blur(radius: 2)
                                            .frame(width: geometry.size.width/4.5 + 16, height: geometry.size.width/4.5 + 16)
                                            .offset(x: 4, y: 4)
                                    )
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color.black.opacity(0.50), lineWidth: 8)
                                            .blur(radius: 2)
                                            .frame(width: geometry.size.width/4.5 + 16, height: geometry.size.width/4.5 + 16)
                                            .offset(x: -4, y: -4)
                                    )
                                    .mask(RoundedRectangle(cornerRadius: 15))
                                
                            }
                            
                            Text(":")
                                .font(.custom("SFProDisplay-Semibold", size:50))
                                .foregroundColor(Color.white)
                            
                            ZStack {
                                CustomPicker(data: minute, selectedItem: self.$selectedPickerMinute, color: "White")
                                    .frame(width: geometry.size.width/4.5, height: (geometry.size.width/4.5)*2, alignment: .center)
                                    .clipped()
                                    .compositingGroup()
                                    .clipShape(RoundedRectangle(cornerRadius: 15))
                                    .onChange(of: self.selectedPickerMinute, perform: { value in
                                        if isTotalOfMillisEqualZero() {
                                            changeSaveDisabledTo(value: true)
                                        }
                                        else {
                                            changeSaveDisabledTo(value: false)
                                        }
                                    })
                                    
                                
                                RoundedRectangle(cornerRadius: 15)
                                    .stroke(Color(color), lineWidth: 8)
                                    .frame(width: geometry.size.width/4.5 + 10, height: geometry.size.width/4.5 + 10, alignment: .center)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color.white.opacity(0.16), lineWidth: 8)
                                            .blur(radius: 2)
                                            .frame(width: geometry.size.width/4.5 + 16, height: geometry.size.width/4.5 + 16)
                                            .offset(x: 4, y: 4)
                                    )
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color.black.opacity(0.50), lineWidth: 8)
                                            .blur(radius: 2)
                                            .frame(width: geometry.size.width/4.5 + 16, height: geometry.size.width/4.5 + 16)
                                            .offset(x: -4, y: -4)
                                    )
                                    .mask(RoundedRectangle(cornerRadius: 15))
                            }
                            
                            
                            Text(":")
                                .font(.custom("SFProDisplay-Semibold", size:50))
                                .foregroundColor(Color.white)
                            
                            ZStack {
                                HStack {
                                    CustomPicker(data: second, selectedItem: self.$selectedPickerSecond, color: "White")
                                        .frame(width: geometry.size.width/4.5, height: (geometry.size.width/4.5)*2, alignment: .center)
                                        .clipShape(RoundedRectangle(cornerRadius: 15))
                                        .compositingGroup()
                                        .clipped()
                                        .onChange(of: self.selectedPickerSecond, perform: { value in
                                            if isTotalOfMillisEqualZero() {
                                                changeSaveDisabledTo(value: true)
                                            }
                                            else {
                                                changeSaveDisabledTo(value: false)
                                            }
                                    })
                                }
                                
                                RoundedRectangle(cornerRadius: 15)
                                    .stroke(Color(color), lineWidth: 8)
                                    .frame(width: geometry.size.width/4.5 + 10, height: geometry.size.width/4.5 + 10, alignment: .center)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color.white.opacity(0.16), lineWidth: 8)
                                            .blur(radius: 2)
                                            .frame(width: geometry.size.width/4.5 + 16, height: geometry.size.width/4.5 + 16)
                                            .offset(x: 4, y: 4)
                                    )
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color.black.opacity(0.50), lineWidth: 8)
                                            .blur(radius: 2)
                                            .frame(width: geometry.size.width/4.5 + 16, height: geometry.size.width/4.5 + 16)
                                            .offset(x: -4, y: -4)
                                    )
                                    .mask(RoundedRectangle(cornerRadius: 15))
                            }
                            .border(Color.red)
                            .clipped()
                            .compositingGroup()
                        }
                        .padding(.bottom, paddingBottomTitle)
                        
                        HStack {
                            ForEach(firstLineColors, id: \.self) { color in
                                Button(action: {
                                    self.color = color
                                }, label: {
                                    if self.color == color {
                                        CustomPickerColor(color: color)
                                            .overlay(
                                                Circle()
                                                    .stroke(Color("\(color)Dark"), lineWidth: 2)
                                            )
                                            .overlay(
                                                Image(systemName: "multiply")
                                                    .resizable()
                                                    .scaledToFit()
                                                    .frame(width: sizePickerColorSelector, height: sizePickerColorSelector, alignment: .center)
                                                    .foregroundColor(.white)
                                            )
                                    } else {
                                        CustomPickerColor(color: color)
                                            .overlay(
                                                Circle()
                                                    .stroke(Color("\(color)Dark"), lineWidth: 2)
                                        )
                                    }
                                })
                                .padding(.horizontal, paddingPickerColor)
                            }
                        }
                        .padding(.init(top: 0, leading: 5, bottom: paddingPickerColor+9, trailing: 5))
                        
                        HStack {
                            ForEach(secondLineColor, id: \.self) { color in
                                Button(action: {
                                    self.color = color
                                }, label: {
                                    if self.color == color {
                                        CustomPickerColor(color: color)
                                            .overlay(
                                                Circle()
                                                    .stroke(Color("\(color)Dark"), lineWidth: 2)
                                            )
                                            .overlay(
                                                Image(systemName: "multiply")
                                                    .resizable()
                                                    .scaledToFit()
                                                    .frame(width: 20, height: 20, alignment: .center)
                                                    .foregroundColor(.white)
                                            )
                                    } else {
                                        CustomPickerColor(color: color)
                                            .overlay(
                                                Circle()
                                                    .stroke(Color("\(color)Dark"), lineWidth: 2)
                                            )
                                    }
                                })
                                .padding(.horizontal, paddingPickerColor)
                            }
                        }
                        .padding(.bottom, paddingBottomTitle)
                        
                        Spacer()
                        
                        if self.isEditMode {
                            HStack {
                                Button(action: {
                                    UIScrollView.appearance().bounces = true
                                    self.presentationMode.wrappedValue.dismiss()
                                }, label: {
                                    Text("Cancel".localized())
                                        .font(.custom("SFProDisplay-Semibold", size:20))
                                        .padding(5)
                                        .foregroundColor(.red)
                                        .frame(width: 110, height: 20, alignment: .center)
                                })
                                .frame(width: buttonWidthSizex2, height: buttonHeightSize, alignment: .center)
                                .buttonStyle(ButtonRoundedRectangleStyle(bgColor: "BackgroundColor", timerColor: "Red", width: buttonWidthSizex2, height: buttonHeightSize))
                                
                                Spacer()
                                
                                Button(action: {
                                    updateTimer()
                                    self.notificationManager.removeNotification()
                                    UIScrollView.appearance().bounces = true
                                    self.presentationMode.wrappedValue.dismiss()
                                }, label: {
                                    Text("Save".localized())
                                        .font(.custom("SFProDisplay-Semibold", size:20))
                                        .padding(5)
                                        .foregroundColor(self.isSaveDisabled ? .gray : .white)
                                        .frame(width: 110, height: 20, alignment: .center)
                                })
                                .frame(width: buttonWidthSizex2, height: buttonHeightSize, alignment: .center)
                                .disabled(self.isSaveDisabled)
                                .buttonStyle(ButtonRoundedRectangleStyle(bgColor: "BackgroundColor", timerColor: self.isSaveDisabled ? "Gray" : self.color, width: buttonWidthSizex2, height: buttonHeightSize))
                            }
                            .padding(.horizontal, 30)
                        } else {
                            Button(action: {
                                addTimer()
                                UIScrollView.appearance().bounces = true
                                self.presentationMode.wrappedValue.dismiss()
                            }, label: {
                                Text("Save".localized())
                                    .font(.custom("SFProDisplay-Semibold", size:20))
                                    .padding(5)
                                    .foregroundColor(self.isSaveDisabled ? .gray : .white)
                                    .frame(width: 130, height: 20, alignment: .center)
                            })
                            .frame(width: buttonWidthSize, height: buttonHeightSize, alignment: .center)
                            .disabled(self.isSaveDisabled)
                            .buttonStyle(ButtonRoundedRectangleStyle(bgColor: "BackgroundColor", timerColor: self.isSaveDisabled ? "Gray" : self.color, width: buttonWidthSize, height: buttonHeightSize))
                        }
                    }
                    //To dismiss keyboard on tap
                    .onTapGesture {
                        textFieldId = UUID().uuidString
                    }
                }
            }
            .onAppear() {
                if self.isEditMode {
                    self.title.value = (timer?.title)!
                }
                print(geometry.size.width/4.5)
                
                UIPickerView.appearance().tintColor = UIColor(Color.clear)
                UISegmentedControl.appearance().selectedSegmentTintColor = UIColor(Color.clear)
                
                UIScrollView.appearance().bounces = false
            }
            .navigationBarHidden(true)
            .ignoresSafeArea(.keyboard)
        }
    }
    
    private func addTimer()
    {
        calculOfMillisecond()
        if title.value == "" { title.value = "Timer".localized() + "\(timersCount + 1)" }
        let newTimer = CustomTimer(context: viewContext)
        newTimer.id = UUID()
        newTimer.title = title.value
        newTimer.nbOfMillisecond = nbOfMillisecond
        newTimer.color = color.description
        saveContext()
    }
    
    private func updateTimer() {
        calculOfMillisecond()
        if title.value == "" { title.value = "Timer".localized() + "\(timersCount + 1)" }
        timer?.title = title.value
        timer?.color = color.description
        timer?.nbOfMillisecond = nbOfMillisecond
        saveContext()
    }
    
    private func isTotalOfMillisEqualZero() -> Bool {
        if selectedPickerHour + selectedPickerMinute + selectedPickerSecond == 0 {
            return true
        } else {
            return false
        }
    }
    
    private func changeSaveDisabledTo(value: Bool) {
        self.isSaveDisabled = value
    }
    
    private func saveContext() {
        do {
            try viewContext.save()
        } catch {
            let error = error as NSError
            fatalError("Unresolved error : \(error)")
        }
    }
    
    private func calculOfMillisecond() {
        let hoursInMillis = selectedPickerHour * 60 * 60 * 100
        let minutesInMillis = selectedPickerMinute * 60 * 100
        let secondsInMillis = selectedPickerSecond * 100
        nbOfMillisecond = Double(hoursInMillis + minutesInMillis + secondsInMillis)
    }
    
    private func calculMillisecondToHour() -> Int {
        return Int(self.timer!.nbOfMillisecond / 360000)
    }
    
    private func calculMillisecondToMinutes() -> Int {
        return (Int(self.timer!.nbOfMillisecond) % 360000) / 6000
    }
    
    private func calculMillisecondToSecond() -> Int {
        return ((Int(self.timer!.nbOfMillisecond) % 360000) % 6000) / 100
    }
}

struct FormTimer_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            AddTimerView(timersCount: 0)
        }
    }
}

class TextLimiter: ObservableObject {
    
    private let limit = 19
    
    @Published var value = "" {
        didSet {
            if value.count > limit && oldValue.count <= limit {
                value = oldValue
            }
        }
    }
}
